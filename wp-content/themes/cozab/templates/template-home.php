<?php
/**
 * Template Name: Home Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Cozab
 * @since Cozab 1.0
 */

get_header();
?>

<body>

		<?php
			wp_body_open();
		?>

		<div class="animsition-overlay" data-animsition-overlay="true">

      <div class="mb_wrap">
        <div class="mb-overlay">
					<div class="mb-overlay-bg"></div>
          <div class="mb-menu">
            <div class="close">
              <ion-icon class="close-mb-menu" name="close-outline"></ion-icon>
						</div>
						
						<!-- your list menu in mobile here -->
            <ul>
             	<li>
                <a href="#" class="has_icon is_active">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path d="M4 16V4H2V2h3a1 1 0 0 1 1 1v12h12.438l2-8H8V5h13.72a1 1 0 0 1 .97 1.243l-2.5 10a1 1 0 0 1-.97.757H5a1 1 0 0 1-1-1zm2 7a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm12 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4z" />
                  </svg>
                  <span> Login </span>
                </a>
							</li>
							
							<!-- Start Menu Header Mobile -->
							<?php 
								get_template_part( 'template-parts/menu-header', null, [
									'position' => 'header-mobile'
								] ); 
							?>
							<!-- End Menu Header Mobile -->
            </ul>
          </div>
        </div>
			</div>
			
			<div class="progress"></div>
			
      <!-- ================================= header -->
      <div class="site-header has_light_dark">
        <header id="masthead" class="site-header header-3 both-types no-transition">
          <div class="container">
            <div class="header-wrap">
              <div class="header-wrap-inner">
                <div class="left-part">
                  <div class="site-branding">
										<div class="site-title">
											<a href="./home" class=" animsition-link">
												<div class="logo">
													<img src="<?=get_template_directory_uri();?>/assets/images/full-white-logo.png" class="svg-logo" alt="LOGO" />
												</div>
											</a>
										</div>
									</div><!-- .site-branding -->
								</div>

								<!-- ============================ right part -->
								<div class="right-part right">
									<nav id="site-navigation" class="main-nav with-counters visible">
										<div class="mbl-overlay ">
											<!-- Navigation -->
											<div id="mega-menu-wrap" class="main-nav-container" role="navigation">
												<ul id="primary-menu" class="menu">
													<!-- Start Menu Header -->
													<?php 
														get_template_part( 'template-parts/menu-header', null, [
															'position' => 'header-desktop'
														] ); 
													?>
													<!-- End Menu Header -->
												</ul>
											</div>
										</div>
									</nav>

									<!-- Fullscreen -->
									<div class="clb-hamburger btn-round btn-round-light" tabindex="1">
										<div class="navbar-menu">
											<a href="" class="btn btn-primary d-none d-md-block  "> Login </a>
										</div>
										<div class="mb_wrap">
											<div class="burger is_white">
												<div class="menu-toggle-icon">
													<div class="cb-menu-toggle">
														<div class="menu">
															<input type="checkbox">
															<div class="line-menu"></div>
															<div class="line-menu"></div>
															<div class="line-menu"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
            </div>
					</div>
        </header>
      </div>
      
      <!-- Content Here -->
			<?php the_content(); ?>
			
      <?php get_footer(); ?>
