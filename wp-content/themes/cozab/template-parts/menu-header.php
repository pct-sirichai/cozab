<?php 
$menus = wp_get_nav_menu_items("Main Menu");
if (!empty($menus) && count($menus) > 0) { 
  foreach ($menus as $iMenu => $menu) {
    if ("header-desktop" === $args['position']) {
?>
      <!-- Menu Item -->
      <li class="mega-menu-item nav-item has-submenu">
        <a href="<?=$menu->url;?>" class="menu-link main-menu-link animsition-link item-title">
          <span class=""><?=$menu->title;?></span>
        </a>
      </li>
<?php 
    } else if ("header-mobile" === $args['position']) { 
?>
      <!-- Menu Item -->
      <li>
        <a href="<?=$menu->url;?>">
          <?=$menu->title;?> <ion-icon name="add-outline" color="light"></ion-icon> 
        </a>
      </li>
<?php 
    } else if ("footer" === $args['position']) { 
?>
      <!-- Menu Item -->
      <li> <a href="<?=$menu->url;?>"><?=$menu->title;?></a> </li>
<?php 
    }
  }
} 
?>
