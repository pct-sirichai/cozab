<?php 
$menus = wp_get_nav_menu_items("Social Menu");
if (!empty($menus) && count($menus) > 0) { 
  foreach ($menus as $iMenu => $menu) {
    if ("footer" === $args['position']) {
?>
      <!-- Menu Item -->
      <li> <a href="<?=$menu->url;?>"><?=$menu->title;?></a> </li>
<?php 
    }
  }
} 
?>
