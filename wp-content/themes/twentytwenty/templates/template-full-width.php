<?php
/**
 * Template Name: Full Width Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

// get_template_part( 'singular' );

if (is_page(33)) {
  get_template_part( 'pages/home' );
} else if (is_page(35)) {
  get_template_part( 'pages/about' );
} else if (is_page(37)) {
  get_template_part( 'pages/contact-us' );
} else {
  get_template_part( 'singular' );
}

