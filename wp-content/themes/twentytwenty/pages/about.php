<?php get_header(); ?>

<div class="spacer_header"></div>

<div class="container">
  <div class="container">
    <div class="hero_wrap">
      <h2 class="hero_title text-center  mb-30"> About Us </h2>
      <p class="hero_description  mb-30 ">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In pretium, elit quis vehicula interdum, sem metus iaculis sapien, sed bibendum lectus augue eumetus. Lorem ipsum dolor sit amea.
      </p>
    </div>
  </div>
</div>

<div class="img_block overflow-hidden">
  <img class="img" src="<?=get_template_directory_uri();?>/assets/images/about.jpeg" alt="">
  <h2 class="in_img"> Cozab </h2>
</div>

<section class="section spacer  is-dark overflow-hidden">
  <div class="container">
    <div class="row align-items-center justify-content-center spacer_bottom">
      <div class="row align-items-center">
        <div class="col-lg-6">
          <div class="heading text-left">
            <h2 class="title subtitle-top w-100">Who We Are ?</h2>
          </div>
        </div>
        <div class="col-lg-6">
          <p class="heading">
            We’re <strong>a team of creatives</strong> who are excited about unique ideas and help fin-tech companies to <strong>create amazing identity</strong> by crafting top-notch UI/UX.
          </p>
        </div>
      </div>
    </div>

    <div class="boxes boxes_reset">
      <div class="row">
        <div class="col-md-3">
          <div class="box has_number">
            <h6 class="number"> THE PROJECT</h6>
            <p class="box-details">
              Our state of the art gyms provide you with a great place to work out in
            </p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="  box has_number">
            <h6 class=" number">OUR PLAN</h6>
            <p class="box-details">
              Our state of the art gyms provide you with a great place to work out in
            </p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="box has_number">
            <h6 class=" number">OUR VALUES</h6>
            <p class="box-details">
              Our state of the art gyms provide you with a great place to work out in
            </p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="box has_number">
            <h6 class=" number">OUR GOALS</h6>
              <p class="box-details">
                Our state of the art gyms provide you with a great place to work out in
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section spacer pt-0  is-dark overflow-hidden">
  <div class="container">
    <div class="heading text-center">
      <h2 class="title subtitle-top">Our team</h2>
    </div>
    <div class="team_block">
      <div class="row">
        <div class="col-lg-4">
          <div class="item">
            <h4 class="name"> Ayoub </h4>
            <img src="<?=get_template_directory_uri();?>/assets/images/team.png" alt="">
          </div>
        </div>
        <div class="col-lg-4">
          <div class="item">
            <h4 class="name"> Ayoub </h4>
            <img src="<?=get_template_directory_uri();?>/assets/images/team1.png" alt="">
          </div>
        </div>
        <div class="col-lg-4">
          <div class="item">
            <h4 class="name"> Ayoub </h4>
            <img src="<?=get_template_directory_uri();?>/assets/images/team2.png" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section spacer is-dark overflow-hidden" >
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6">
        <div class="heading">
            <h2 class="title subtitle-top"> Agency proud is quuality <br> of partners </h2>
        </div>
      </div>
      <div class="col-lg-6">
        <div class=" section  section-companies has-style1 is-dark">
          <div class="row min-30 flex center">
            <div class="col-lg-4 col-md-3  col-6">
              <div class="company-item">
                <img src="<?=get_template_directory_uri();?>/assets/images/clients-logos/envato.svg" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-6">
              <div class="company-item">
                <img src="<?=get_template_directory_uri();?>/assets/images/clients-logos/amazon.svg" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-6">
              <div class="company-item">
                  <img src="<?=get_template_directory_uri();?>/assets/images/clients-logos/netflix.svg" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-6">
              <div class="company-item">
                <img src="<?=get_template_directory_uri();?>/assets/images/clients-logos/shutterstock.svg" alt="">
              </div>
            </div>
            <div class="col-lg-4 col-md-3 col-6">
              <div class="company-item">
                <img src="<?=get_template_directory_uri();?>/assets/images/clients-logos/500.svg" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section spacer overflow-hidden is-dark bg-gray">
  <div class="container">
    <div class="call2action">
      <div class=" d-md-flex justify-content-between d-block ">
        <h2 class="title subtitle-top"> Start you business with us </h2>
        <div>
          <a href="" class="btn btn-white"> Contact us </a>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>