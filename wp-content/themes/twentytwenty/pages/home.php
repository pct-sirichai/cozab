<?php
/**
 * The template for displaying single posts and pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<div class="hero has-style1 jarallax" style=" background-image: url(<?=get_template_directory_uri();?>/assets/images/bg.jpg); ">
	<div class="vertical_social">
		<a href="">
			<strong>facebook</strong>
		</a>
		<a href="">
			<strong>instagram</strong>
		</a>
		<a href="">
			<strong>twitter</strong>
		</a>
	</div>
	
	<div class="spacer_header"></div>
	
	<div class="hero_wrap">
		<div class="heading text-center">
			<h1 class="title">
				<span class="line-text"> Cozab</span><br> media placement<span class="brand-color">.</span> 
			</h1>
		</div>
	</div>
	
	<div class="container"></div>
</div>

<section class="section spacer is_dark bg-gray">
	<div class="container">
		<div class="row align-items-center justify-content-center spacer_bottom">
			<div class="col-md-5 mb-20 mt-20">
				<div class="heading text-left">
					<p class="subtitle text-secondary">About Cozab</p>
					<h2 class="title subtitle-top text-white">
						A digital agency with a difference<span class="brand-color">.</span>
					</h2>
				</div>
				<p class="heading">We have a visionary group of professionals that collectively form a team to provide leading digital services to our clientsto help maximise their potential online. We have no geographical boundaries in sourcing &amp; recruiting the worlds finest talent to supply our customers with the best practice from around the world. Our team brings decades of experience in the online industry &amp; they collectively work together to deliver digital solutions for customers who strive for a stronger digital presence.</p>
				<a href="" class="btn btn-primary btn-sm"> About Us </a>
			</div>
			<div class="col-md-7">
				<div class="3d-wrap">
					<img class="img-fluid 3d-hover" src="<?=get_template_directory_uri();?>/assets/images/decoration.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section spacer is-dark ">
	<div class="container">
		<div class="row mb-5">
			<div class="col-md-12 d-flex justify-content-between align-items-center">
				<div class="heading text-left">
					<p class="subtitle text-secondary">Services</p>
					<h2 class="title subtitle-top">Our global team have the ability to service international or local clients around the world, working around the clock, in different time zones conversing in different languages.</h2>
				</div>
			</div>
		</div>
		<div class="boxes boxes_reset">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="box has_icon">
						<div class="box-icon">
							<ion-icon name="search-outline"></ion-icon>
						</div>
						<h4 class="box-title">Market Research</h4>
						<p class="box-desc">We provide advice, recommendations, planning and implementation guidance.</p>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="box has_icon is_active">
						<div class="box-icon">
							<ion-icon name="chatbubbles-outline"></ion-icon>
						</div>
						<h4 class="box-title">
							Collaboration Request
						</h4>
						<p class="box-desc">
							We provide advice, recommendations, planning and implementation guidance.
						</p>
					</div>
				</div>
				
				<div class="col-md-4 col-sm-6">
					<div class="box has_icon">
						<div class="box-icon">
							<ion-icon name="location-outline"></ion-icon>
						</div>
						<h4 class="box-title">Secured Media Placement</h4>
						<p class="box-desc">We provide advice, recommendations, planning and implementation guidance.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section spacer pb-0 is-dark bg-gray">
	<div class="container">
		<div class="testimonial-box">
			<div class="box-left">
				<img src="<?=get_template_directory_uri();?>/assets/images/hero-1.jpg" alt="">
			</div>
			<div class="box-right bg-pattern ">
				<p class="testimonial-quote">“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.”</p>
				<h4 class="title"> Edward Kennedy, The Percentage company </h4>
				<a href="" class="btn btn-white btn-outline "> See our Case Studies </a>
			</div>
		</div>
	</div>
</section>

<section class="section spacer is-dark">
	<div class="hero_page has_style1">
		<div class="container">
			<div class="hero_wrap">
				<h1 class="hero_title text-left  ml-0"> Contact Us</h1>
				<h2 class="outline_text"> LET'S TALK </h2>
			</div>
		</div>
	</div>
	<div class="block_contact_form">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="content">
						<form>
							<div class="form-row">
								<div class="col col-lg-12 col-md-12 col-sm-12">
									<div class="form-group">
										<label for="nameInput">Name *</label>
										<input type="text" class="form-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
									</div>
								</div>
							</div>
							
							<div class="form-row">
								<div class="col col-lg-12 col-md-12 col-sm-12">
									<div class="form-group">
										<label for="emailInput">Email *</label>
										<input type="email" class="form-control">
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label for="messagesInput">Tell us about your project *</label>
								<textarea id="messagesInput" cols="30" rows="3" class="form-control"></textarea>
							</div>
							
							<button type="button" class="btn btn-primary">Send Message</button>
						</form>
					</div>
				</div>
				
				<div class="col-lg-6">
					<div class="card_grad">
						<h4>ADDRESS</h4>
						<p>Cozab. Ratsada, Mueang Phuket, District, Phuket 83000. Thailand.</p>
					</div>
					<div class="card_grad">
						<h4>EMAIL</h4>
						<p>jack@cozab.com</p>
					</div>
					<div class="card_grad">
						<h4>PHONE</h4>
						<p>+66 (0) 960 562 518 </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
