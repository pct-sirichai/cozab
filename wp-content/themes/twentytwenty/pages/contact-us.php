<?php get_header(); ?>

<div class="spacer_header"></div>

<div class="container">
  <div class="hero_wrap">
    <h1 class="hero_title text-left  ml-0"> Contact Us </h1>
    <h2 class="outline_text">LET'S TALK</h2>
  </div>
</div>

<div class="lines_effect ">
    <img src="../assets/images/others/lines.png" alt="">
</div>

<div class="block_contact_form">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="content">
          <form>
            <div class="form-row">
              <div class="col col-lg-12 col-md-12 col-sm-12">
                <div class="form-group">
                  <label for="nameInput">Name *</label>
                  <input type="text" class="form-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="col col-lg-12 col-md-12 col-sm-12">
                <div class="form-group">
                  <label for="emailInput">Email *</label>
                  <input type="email" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="messagesInput">Tell us about your project *</label>
              <textarea id="messagesInput" cols="30" rows="3" class="form-control"></textarea>
            </div>
            <button type="button" class="btn btn-primary">Send Message</button>
          </form>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card_grad">
          <h4>ADDRESS</h4>
          <p>Cozab. Ratsada, Mueang Phuket, District, Phuket 83000. Thailand.</p>
        </div>
        <div class="card_grad">
          <h4>EMAIL</h4>
          <p>jack@cozab.com</p>
        </div>
        <div class="card_grad">
          <h4>PHONE</h4>
          <p>+66 (0) 960 562 518 </p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="section spacer is-dark bg-gray">
  <div class="container">
    <div class="call2action ">
      <div class=" d-md-flex justify-content-between d-block ">
        <h2 class="title subtitle-top">Find out more about us </h2>
        <div><a href="about.html" class="btn btn-white"> About us </a></div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>