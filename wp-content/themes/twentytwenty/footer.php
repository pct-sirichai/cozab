<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

			<footer class="footer">
        <div class="footer-body">
          <div class="container">
            <div class="row ">
              <div class="col-lg-4">
                <div class="footer-desc">
                  <img src="<?=get_template_directory_uri();?>/assets/images/full-white-logo.png" height="200" alt="logo">
                </div>
              </div>
              <div class="col-lg-4 col-6">
                <h6 class="list-title">Contact</h6>
                <ul class="list-items">
                  <li> Cozab. Ratsada, Mueang Phuket</li>
                  <li> District, Phuket 83000. Thailand.</li>
                  <li> jack@cozab.com</li>
									<li> +66 (0) 960 562 518</li>
                </ul>
                </div>
                <div class="col-lg-2 col-6">
                  <h6 class="list-title">Links</h6>
                  <ul class="list-items">
                    <li> <a href="#">About</a> </li>
                    <li> <a href="#">Services</a> </li>
                    <li> <a href="#">Blog</a> </li>
										<li> <a href="#">Contact</a> </li>
                  </ul>
                </div>
                <div class="col-lg-2">
                  <h6 class="list-title">Follow Us</h6>
                  <ul class="list-items">
                    <li> <a href="#">Facebook</a> </li>
                    <li> <a href="#">Instagram</a> </li>
                    <li> <a href="#">Twitter</a> </li>
										<li> <a href="#">Youtube</a> </li>
                  </ul>
                </div>
              </div>
          </div>
          <p class="copyright text-center text-copyright">&#169; 2020 Cozab.</p>
        </div>
      </footer>
    </div>

		<?php wp_footer(); ?>

	</body>
</html>
