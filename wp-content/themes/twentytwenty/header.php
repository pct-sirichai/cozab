<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
			wp_body_open();
		?>

		<div class="animsition-overlay" data-animsition-overlay="true">

      <div class="mb_wrap">
        <div class="mb-overlay">
					<div class="mb-overlay-bg"></div>
          <div class="mb-menu">
            <div class="close">
              <ion-icon class="close-mb-menu" name="close-outline"></ion-icon>
						</div>
						
						<!-- your list menu in mobile here -->
            <ul>
             	<li>
                <a href="#" class="has_icon is_active">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill="none" d="M0 0h24v24H0z" />
                    <path d="M4 16V4H2V2h3a1 1 0 0 1 1 1v12h12.438l2-8H8V5h13.72a1 1 0 0 1 .97 1.243l-2.5 10a1 1 0 0 1-.97.757H5a1 1 0 0 1-1-1zm2 7a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm12 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4z" />
                  </svg>
                  <span> Login </span>
                </a>
              </li>
              <li>
								<a class="has-dropdown-m " href="./home">
									Home <ion-icon name="add-outline"></ion-icon> 
								</a>
              </li>
							<li>
								<a class="has-dropdown-m " href="./about">
									About <ion-icon name="add-outline"></ion-icon>
								</a>
              </li>
							<li>
								<a class="has-dropdown-m" href="#blog">
									Blog <ion-icon name="add-outline"></ion-icon>
								</a>
              </li>
							<li>
								<a class="has-dropdown-m" href="./contact-us">
									Contact <ion-icon name="add-outline"></ion-icon>
								</a>
              </li>
            </ul>
          </div>
        </div>
			</div>
			
			<div class="progress"></div>
			
      <!-- ================================= header -->
      <div class="site-header has_light_dark ">
        <header id="masthead" class="site-header header-3 both-types no-transition">
          <div class="container">
            <div class="header-wrap">
              <div class="header-wrap-inner">
                <div class="left-part">
                  <div class="site-branding">
										<div class="site-title">
											<a href="./index.html" class=" animsition-link">
												<div class="logo">
													<img src="<?=get_template_directory_uri();?>/assets/images/full-white-logo.png" class="svg-logo" alt="LOGO" />
												</div>
											</a>
										</div>
									</div><!-- .site-branding -->
								</div>

								<!-- ============================ right part -->
								<div class="right-part right">
									<nav id="site-navigation" class="main-nav with-counters visible">
										<div class="mbl-overlay ">
											<!-- Navigation -->
											<div id="mega-menu-wrap" class="main-nav-container" role="navigation">
												<ul id="primary-menu" class="menu">
													<!-- ======================== top link -->
													<li class="mega-menu-item nav-item -0 has-submenu">
														<a href="./home" class="menu-link  main-menu-link  animsition-link item-title">
															<span class="">Home</span>
														</a>
													</li>
													<!-- ======================== top link -->
													<li class="mega-menu-item nav-item -0 has-submenu">
														<a href="./about" class="menu-link  animsition-link main-menu-link  animsition-link item-title">
															<span class="">About Us</span>
														</a>
													</li>
													<!-- ======================== top link -->
													<li class="mega-menu-item nav-item -0 has-submenu">
														<a href="" class="menu-link  animsition-link main-menu-link  animsition-link item-title">
															<span class="">Blog</span>
														</a>
													</li>
													<!-- ======================== top link -->
													<li class="mega-menu-item nav-item -0 has-submenu">
														<a href="./contact-us" class="menu-link  animsition-link main-menu-link  animsition-link item-title" data-aos="fade-right" data-aos-delay="200">
															<span class="">Contact</span>
														</a>
													</li>
												</ul>
											</div>
										</div>
									</nav>

									<!-- Fullscreen -->
									<div class="clb-hamburger btn-round btn-round-light" tabindex="1">
										<div class="navbar-menu">
											<a href="" class="btn btn-primary d-none d-md-block  "> Login </a>
										</div>
										<div class="mb_wrap">
											<div class="burger is_white">
												<div class="menu-toggle-icon">
													<div class="cb-menu-toggle">
														<div class="menu">
															<input type="checkbox">
															<div class="line-menu"></div>
															<div class="line-menu"></div>
															<div class="line-menu"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
            </div>
					</div>
				</header>
    	</div>

		<?php
